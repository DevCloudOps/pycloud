found 3 python git modules:

gitpython -- https://github.com/gitpython-developers/GitPython
gittle -- https://github.com/FriendCode/gittle
pygithub -- https://github.com/PyGithub/PyGithub

Gitpython seems to provide the simplest effecictive clone func ala:

import git
res = git.Repo.clone_from(url, localdir, branch=branch, recursive=True, depth=1)

also, using pexpect to run the anaconda install bash script may be a challenge,
so just run it manually and proceed to use its install path for openstack.
https://docs.anaconda.com/anaconda/install/linux

for example, assuming anaconda python has been installed in /opt/osqueens:
/opt/osqueens/bin/python ./osqueens.py
