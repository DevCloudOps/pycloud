#!/usr/bin/env python

# http://python-future.org/quickstart.html
from __future__ import (absolute_import, division, print_function, unicode_literals)

import os, platform, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

"""
https://medium.freecodecamp.org/python-list-comprehensions-vs-generator-expressions-cef70ccb49db
what is the difference between the following syntax?
mut = [x for x in range(5)]
gen = (x for x in range(5))
con = tuple(range(5))
"""

def sample_listcomprehend():
  """
  https://www.datacamp.com/community/tutorials/python-list-comprehension
  https://docs.python.org/2.7/tutorial/datastructures.html?highlight=list%20comprehension
  """
  from math import pi
  spi = [str(round(pi, i)) for i in range(1, 6)]
  # ['3.1', '3.14', '3.142', '3.1416', '3.14159']

  # list comprehension will transpose rows and columns:
  m = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
  tm = [[row[i] for row in matrix] for i in range(4)]
  # [[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]

  # use list to manip dict --  `fahrenheit` dictionary 
  fahrenheit = {'t1':-30, 't2':-20, 't3':-10, 't4':0}

  # corresponding `celsius` values via an anonymous lambda
  celsius = list(map(lambda x: (float(5)/9)*(x-32), fahrenheit.values()))

  # the `celsius` dictionary
  celsius_dict = dict(zip(fahrenheit.keys(), celsius))

def sample_dictcomprehend():
  """
  https://www.datacamp.com/community/tutorials/python-dictionary-comprehension
  """
  # rewrite temperature conversion algo ...
  # use dict comprehension istead of lamdathe `fahrenheit` dictionary 
  fahrenheit = {'t1': -30,'t2': -20,'t3': -10,'t4': 0}

  # corresponding `celsius` values and create the new dictionary
  cf = 5.0/9.0
  celsius = {k:(cf*(v-32)) for (k,v) in fahrenheit.items()}

def sample_generator():
  """
  https://anandology.com/python-practice-book/iterators.html
  """
  def check_prime(number):
    for divisor in range(2, int(number ** 0.5) + 1):
      if number % divisor == 0:
        return False
    return True

  p = (i for i in range(2, 100) if check_prime(i))
  a = (x*x for x in range(100))
  pa = dict(zip(a, None))
  pa = {k:(check_prime(k) for (k,v) in pa.items()} 

if __name__ == '__main__':
  print(sys.version_info)
  print(platform.uname())
  print(platform.python_implementation(), platform.python_version_tuple())
  for key,val in os.environ.items(): print(key,'==',val)

