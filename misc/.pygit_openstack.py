#!/usr/bin/env python
# http://pygithub.readthedocs.io/en/latest/introduction.html#very-short-tutorial
# https://sookocheff.com/post/tools/downloading-directories-of-code-from-github-using-the-github-api/

# http://python-future.org/quickstart.html
from __future__ import (absolute_import, division, print_function, unicode_literals)
import os, platform, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

from github import Github

_access_token = "95468531d67f2d6af39b05867b44988dedcc1e4a"

def repolist(g):
  for repo in g.get_user().get_repos():
    print(repo.name)
    # repo.edit(has_wiki=False)

def download_dir(repo, sha, path)
  contents = repo.get_dir_contents(server_path, ref=sha)

  for content in contents:
    print "Processing %s" % content.path
      if content.type == 'dir':
        download_dir(repository, sha, content.path)
        return
      try:
        path = content.path
        file_content = repository.get_contents(path, ref=sha)
        file_data = base64.b64decode(file_content.content)
        file_out = open(content.name, "w")
        file_out.write(file_data)
        file_out.close()
      except (GithubException, IOError) as exc:
        logging.error('Error processing %s: %s', content.path, exc)

def clone_branch(org, repo_name, branch='stable/queens'):
# https://sookocheff.com/post/tools/downloading-directories-of-code-from-github-using-the-github-api/
  """
  Download all contents at server_path with commit branch sha in the repository.
  """

  repo = org.get_repo(repo_name)
  branches = repo.get_branches()
  sha = None
  if matched_branches: sha = matched_branches[0].commit.sha
  if sha:
    download_dir(repo, sha, path)

def runtime():
  print(sys.version_info)
  print(platform.platform()) ; print(platform.uname())
  print(platform.python_implementation(), platform.python_version_tuple())

  envdict = os.environ.items()
  envenum = enumerate(envdict)
  envdict = { k: v for k, v in envdict if not k.startswith('LS_') }
  # for k, v in envenum: print(k,'==',v)
  for k in envdict: print(k,'==',envdict[k])

  envenum = enumerate(envdict)
  # for k, v in envdict: print(k,'==',v)
        
if __name__ == '__main__':
  runtime()
  #g = Github("user", "password")
  g = Github(_access_token)
  repolist(g)
  org = g.get_user().get_orgs()[0]

