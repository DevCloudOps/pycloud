#!/usr/bin/env python

# http://python-future.org/quickstart.html
from __future__ import (absolute_import, division, print_function, unicode_literals)

import os, platform, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

def runtime(info=[], clear=True):
  if(clear and len(info)> 0): del info[:]
  if(len(info) > 0): return len(info)
  info.append(platform.platform()) # ; print(platform.platform())
  info.append(platform.python_implementation()) # ; print(platform.python_implementation())
  info.append(sys.version_info) # ; print(sys.version_info)
  # info.append(platform.python_version_tuple()) ; print(platform.python_version_tuple())
  info.append(platform.uname()) # ; print(platform.uname())

  envdict = os.environ.items()
  envenum = enumerate(envdict)

  envdict = { k: v for k, v in envdict if not k.startswith('LS_') }
  envenum = enumerate(envdict)

  info.append(envdict)
 
  # for k in envdict: print(k,'==',envdict[k])
  # for k, v in envenum: print(k,'==',v)
  # for k, v in envdict: print(k,'==',v)
  return len(info)
        
if __name__ == '__main__':
  info = []
  ilen = runtime(info)
  ilen = runtime(info, clear=False)
  for idx in range(ilen): print(idx, info[idx])

