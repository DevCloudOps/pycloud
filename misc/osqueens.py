#!/usr/bin/env python
# https://gitpython.readthedocs.io/en/stable/intro.html

# http://python-future.org/quickstart.html
from __future__ import (absolute_import, division, print_function, unicode_literals)
import os, platform, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

def repodict():
  repos = dict(keystone='https://github.com/openstack/keystone.git',
               cinder='https://github.com/openstack/cinder.git',
               glance='https://github.com/openstack/glance.git',
               neutron='https://github.com/openstack/neutron.git',
               nova='https://github.com/openstack/nova.git',
               heat='https://github.com/openstack/heat.git',
               magnum='https://github.com/openstack/magnum.git',
               horizon='https://github.com/openstack/horizon.git')
  return repos

def clone_recur(url, localpath, release='stable/queens'):
  # https://stackoverflow.com/questions/31828308/gitpython-clone-repository-error
  # https://github.com/gitpython-developers/GitPython/issues/269
  import git
  res = git.Repo.clone_from(url, localpath, branch=release, recursive=True, depth=1)
  return res

def clone_all(repos, release='stable/queens'):
  allres = {}
  for name in repos.keys():
    url = repos[name] ; path = './tmp/'+name ; print(path, url)
    res = clone_recur(url, path, release)
    allres[path] = res

  return allres

def install_all(repos, clonepath='./tmp', release='stable/queens'):
  """
  latest anaconda python 2.7 for openstack python 
  https://repo.continuum.io/archive/Anaconda2-5.1.0-Linux-x86_64.sh
  presumably installed in /opt/osqueens
  """
  from distutils.core import run_setup
  import pexpect
  cwd = os.getcwd() ; print(cwd)
  cmd = pyconda = '/opt/osqueens/bin/python '
  cmd += 'install' # 'build'
  for name in repos.keys():
    os.chdir(clonepath+'/'+name)
    log = pexpect.run(cmd)
    # log = run_setup('./setup.py', cmd, stop_after='run')
    os.chdir(cwd)
    print(log)

if __name__ == '__main__':
  args = sys.argv[1:]
  repos = repodict()
  # res = clone_all(repos)
  install_all(repos)

