#!/usr/bin/env python

# http://python-future.org/quickstart.html
from __future__ import (absolute_import, division, print_function, unicode_literals)

import os, platform, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import pexpect

def conda2(path='/opt/conda2-5.1.0'):
  child = pexpect.spawn('bash Anaconda2-5.1.0-Linux-x86_64.sh')
  child.expect('Please, press ENTER to continue')
  child.sendline(' ')

  child.expect('Do you accept the license terms? [yes|no]')
  child.sendline('yes')
  
  child.expect('Anaconda2 will now be installed into this location:')
  child.sendline(path)
        
if __name__ == '__main__':
  conda2()

