#!/usr/bin/env python

import hashlib, logging
from flask import Flask, jsonify
#from flask_debugtoolbar import DebugToolbarExtension

app = Flask(__name__)
app.debug = True
app.logger.setLevel(logging.DEBUG)
logger = app.logger

#logging.basicConfig()
#logger = logging.getLogger(__name__)
#logger.setLevel(logging.DEBUG)
#app.logger = logger
#toolbar = DebugToolbarExtension(app)

@app.route('/', methods=['GET', 'POST'])
def lambda_handler(event=None, context=None):
  endpnt = '/'
  logger.info("lambda info logger.for "+endpnt)
  logger.debug("debug logger.for "+endpnt)
  logger.error("error logger.for "+endpnt)
  return jsonify({'hello': 'world of AWS zappa-flask'})

# a few more examples:

@app.route('/hello/{name}', methods=['GET', 'POST'])
def hello_name(name):
  # '/hello/james' -> {"hello": "james"}
  endpnt = '/hello/'
  logger.info("info logger.for "+endpnt)
  logger.debug("debug logger.for "+endpnt)
  logger.error("error logger.for "+endpnt)
  return jsonify({'hello': name})
 
@app.route('/usr', methods=['GET', 'POST'])
def create_user():
  # This is the JSON body the user sent in their POST request.
  # user_as_json = app.current_request.json_body
  # We'll echo the json body back to the user in a 'user' key.
  endpnt = '/usr'
  logger.info("info logger.for "+endpnt)
  logger.debug("debug logger.for "+endpnt)
  logger.error("error logger.for "+endpnt)
  return jsonify({'user': user_as_json})
#
# See the README documentation for more examples.
#
@app.route('/md5/{string_to_hash}', methods=['GET', 'POST'])
def hash_it(string_to_hash):
  endpnt = '/md5/'
  logger.info("info logger.for "+endpnt)
  logger.debug("debug logger.for "+endpnt)
  logger.error("error logger.for "+endpnt)
  utf8 = string_to_hash.encode("UTF-8")
  m = hashlib.md5()
  m.update(utf8)
  result = m.hexdigest()
  return jsonify({'md5 result of '+utf8: result})

# for local development.
if __name__ == '__main__':
  app.run(debug=True)

