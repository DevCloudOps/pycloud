#!/usr/bin/env python
from __future__ import absolute_import, division, print_function

from chalice import Chalice
import md5, logging

# from: https://hackernoon.com/creating-a-serverless-python-api-using-aws-lambda-chalice-d321dc43ce2
app = Chalice(app_name='chalice_md5_v0.0')
app.debug = True
# Enable DEBUG logs.
app.log.setLevel(logging.DEBUG)

@app.route('/')
def index():
  endpnt = '/'
  app.log.info("info log for "+endpnt)
  app.log.debug("debug log for "+endpnt)
  app.log.error("error log for "+endpnt)
  return {'hello': 'world of AWS chalice'}

# The view function above will return {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
@app.route('/hello/{name}')
def hello_name(name):
  # '/hello/james' -> {"hello": "james"}
  endpnt = '/hello/'
  app.log.info("info log for "+endpnt)
  app.log.debug("debug log for "+endpnt)
  app.log.error("error log for "+endpnt)
  return {'hello': name}
 
@app.route('/usr', methods=['POST'])
def create_user():
  # This is the JSON body the user sent in their POST request.
  # user_as_json = app.current_request.json_body
  # We'll echo the json body back to the user in a 'user' key.
  endpnt = '/usr'
  app.log.info("info log for "+endpnt)
  app.log.debug("debug log for "+endpnt)
  app.log.error("error log for "+endpnt)
  return {'user': user_as_json}
#
# See the README documentation for more examples.
#
@app.route('/md5/{string_to_hash}')
def hash_it(string_to_hash):
  endpnt = '/md5/'
  app.log.info("info log for "+endpnt)
  app.log.debug("debug log for "+endpnt)
  app.log.error("error log for "+endpnt)
  utf8 = string_to_hash.encode("UTF-8")
  m = md5.new()
  m.update(utf8)
  result = m.hexdigest()
  return {'md5 result of '+utf8: result}

