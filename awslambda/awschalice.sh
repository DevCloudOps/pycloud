#!/bin/sh

which python2.7
which virtualenv

virtualenv -p python2.7 py2chalice
. py2chalice/bin/activate

pip install chalice
chalice new-project chalice_md5
pushd chalice_md5

chalice deploy --stage dev
chalice url --stage dev

chalice deploy --stage prod
chalice url --stage prod

chalice delete --stage prod
chalice delete --stage dev


